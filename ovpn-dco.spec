%global _hardened_build 1
%global debug_package %{nil}

%define  dkms_name ovpn-dco

Name:       %{dkms_name}
Version:    0.2.20240619
Release:    1%{?dist}
Summary:    OpenVPN Data Channel Offload kernel accelerator
License:    GPLv2
URL:        https://github.com/OpenVPN/ovpn-dco
Source0:    https://swupdate.openvpn.net/community/releases/%{dkms_name}-%{version}%{?versiontag}.tar.gz
Source1:    https://swupdate.openvpn.net/community/releases/%{dkms_name}-%{version}%{?versiontag}.tar.gz.asc
Source2:    dkms.conf.in
Source3:    gpgkey-F554A3687412CFFEBDEFE0A312F5F7B42F2B01E7.gpg
BuildArch:  noarch

BuildRequires: make
BuildRequires: kernel-devel
BuildRequires: gnupg2

%description
OpenVPN Data Channel Offload kernel module

%package -n kmod-%{dkms_name}
Summary:    OpenVPN Data Channel Offload kernel accelerator (DKMS package)
Requires:   dkms >= 1.95
Requires:   kernel-devel >= 4.18.0
Requires:   kernel >= 4.18.0

%description -n kmod-%{dkms_name}
This package contains an kernel module to accelerate OpenVPN traffic by doing
the data channel encryption and decryption directly in the kernel space and
put it directly unto a virtual network interface.  By avoiding going via the
user-space (which the traditional TUN/TAP devices requires), the encrypted
data path from the remote host to the unencrypted data on the local virtual
interface is shorter and more direct.

Beware that the ovpn-dco kernel module does not support all the same data
channel ciphers OpenVPN may support using a TUN/TAP device.  This is because
all the encryption/decryption of data channel packets utilizes the crypto API
available directly in the Linux kernel and does not involve OpenSSL.  Only
the control channel packets are handled in user-space, where OpenSSL is used
for the TLS packet handling.

%prep
gpgv2 --quiet --keyring %{SOURCE3} %{SOURCE1} %{SOURCE0}
%autosetup

%build
# Nothing to build

%install
if [ "%{buildroot}" != "/" ]; then
        rm -rf %{buildroot}
fi
mkdir -p %{buildroot}/%{_prefix}/src/
mkdir -p %{buildroot}/%{_datarootdir}/%{module_name}
cp -rv ../%{dkms_name}-%{version} %{buildroot}/%{_prefix}/src
rm -f %{buildroot}/%{_prefix}/src/%{dkms_name}-%{version}/.gitignore

# Prepare dkms.conf
cat %{SOURCE2} | sed -e 's/@@VERSION@@/%{version}.%{release}/' > %{buildroot}/%{_prefix}/src/%{dkms_name}-%{version}/dkms.conf

# Rename ovpn-dco source dir to the package version including distro release tag
mv %{buildroot}/%{_prefix}/src/%{dkms_name}-%{version} %{buildroot}/%{_prefix}/src/%{dkms_name}-%{version}.%{release}

mkdir -p %{buildroot}/%{_pkgdocdir}
install -m644 README %{buildroot}/%{_pkgdocdir}

if [ -f "%{name}-%{version}.dkms.tar.gz" ]; then
        install -m 644 %{dkms_name}-%{version}.dkms.tar.gz %{buildroot}/%{_datarootdir}/%{dkms_name}
fi

%clean
if [ "%{buildroot}" != "/" ]; then
        rm -rf %{buildroot}
fi

%post -n kmod-%{dkms_name}
# we cannot use %%{_libdir} as that points at /usr/lib64 on 64-bit platforms
for POSTINST in %{_prefix}/lib/dkms/common.postinst %{_datarootdir}/%{dkms_name}/postinst; do
        if [ -f $POSTINST ]; then
                $POSTINST %{dkms_name} %{version}.%{release} %{_datarootdir}/%{dkms_name}
                exit $?
        fi
        echo "WARNING: $POSTINST does not exist."
done
echo -e "ERROR: DKMS version is too old and %{dkms_name} was not"
echo -e "built with legacy DKMS support."
echo -e "You must either rebuild %{dkms_name} with legacy postinst"
echo -e "support or upgrade DKMS to a more current version."
exit 1

%preun -n kmod-%{dkms_name}
echo -e
echo -e "Uninstall of %{dkms_name} module (version %{version}.%{release}) beginning:"
dkms remove -m %{dkms_name} -v %{version}.%{release} --all --rpm_safe_upgrade
exit 0

%files -n kmod-%{dkms_name}
%{_pkgdocdir}
%{_prefix}/src/%{dkms_name}-%{version}.%{release}

%changelog
* Thu Jun 20 2024 Frank Lichtenheld <frank.lichtenheld@openvpn.net> - 0.2.20240619-1
- Update to ovpn-dco release v0.2.20240619

* Thu Nov 16 2023 David Sommerseth <davids@openvpn.net> - 0.2.20231117-1
- Update to ovpn-dco release v0.2.20231117

* Wed Oct 11 2023 David Sommerseth <davids@openvpn.net> - 0.2.20231010-1
- Update to ovpn-dco release v0.2.20231010

* Fri Oct 6 2023 David Sommerseth <davids@openvpn.net> - 0.2.20230426-3
- Fix incorrect dkms.conf.in variables; kernel module is now named ovpn-dco-v2

* Fri Oct 6 2023 David Sommerseth <davids@openvpn.net> - 0.2.20230426-2
- Add PGP signature check of source code

* Fri Oct 6 2023 David Sommerseth <davids@openvpn.net> - 0.2.20230426-1
- Update to ovpn-dco release v0.2.20230426

* Mon Sep 5 2022 David Sommerseth <davids@openvpn.net> - 0-20220905git3ba6c07
- Update to ovpn-dco master commit 3ba6c07ababd4d491c77f5327f8105bead6ddccc

* Mon May 23 2022 David Sommerseth <davids@openvpn.net> - 0-20220421git2db65af
- Update to ovpn-dco master commit 2db65afe77f9857f5c6784091cff8d6fd8395d34

* Fri Mar 18 2022 David Sommerseth <davids@openvpn.net> - 0-20220318gitd0d9920
- Update to ovpn-dco master commit d0d992019fa07a099fd449e360dfc21a0d1294c6

* Wed Jan 26 2022 David Sommerseth <davids@openvpn.net> - 0-20220126gitca4de3d
- Update to ovpn-dco master commit ca4de3d5f367a8f84e6f783f780767bafad75e6e

* Wed Jul 7 2021 David Sommerseth <davids@openvpn.net> - 0-20210708git6ae3b85
- Update to ovpn-dco master commit 6ae3b85f7bee7d9435dc0404868929bf71d97703

* Thu Oct 29 2020 David Sommerseth <davids@openvpn.net> - 0-20201029gited1ca44
- Update to ovpn-dco master commit ed1ca44410336bee1e81c6985c54f01520256ec5
- Explicitly pass KERNEL_SRC as part of the make call, including the proper kernel version
- Change the ovpn-dco DKMS version string to match the RPM package version/release; to avoid uninstalling DKMS on upgrades
